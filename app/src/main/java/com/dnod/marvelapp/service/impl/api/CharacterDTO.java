package com.dnod.marvelapp.service.impl.api;

public class CharacterDTO {
    private String id;
    private String description;
    private String name;
    private Thumbnail thumbnail;

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public CharacterDTO setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
        return this;
    }

    public String getId() {
        return id;
    }

    public CharacterDTO setId(String id) {
        this.id = id;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public CharacterDTO setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getName() {
        return name;
    }

    public CharacterDTO setName(String name) {
        this.name = name;
        return this;
    }
}
