package com.dnod.marvelapp.service.impl.api;

import java.util.List;

public class Result {
    protected int code;
    protected String status;
    protected String message;
    protected String copyright;
    protected String attributionText;
    protected String attributionHTML;
    protected String etag;
    protected Data data;

    protected static class Data {
        protected List<CharacterDTO> results;
    }
}
