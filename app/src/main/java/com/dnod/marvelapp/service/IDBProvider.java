package com.dnod.marvelapp.service;

import com.dnod.marvelapp.data.Character;

import java.util.List;

public interface IDBProvider {

    List<Character> getLatestCharacters(int limit);

    boolean saveCharacter(Character character);
}
