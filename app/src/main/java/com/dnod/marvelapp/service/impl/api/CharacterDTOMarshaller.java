package com.dnod.marvelapp.service.impl.api;

import com.dnod.marvelapp.data.Character;
import com.dnod.marvelapp.service.Marshaller;

import java.io.File;

public class CharacterDTOMarshaller extends Marshaller<Character, CharacterDTO> {

    private final String imageSize;

    public CharacterDTOMarshaller(String imageSize) {
        this.imageSize = imageSize;
    }

    @Override
    public CharacterDTO fromEntity(Character entity) {
        return new CharacterDTO().setDescription(entity.getDescription())
                .setId(entity.getId())
                .setName(entity.getName());
    }

    @Override
    public Character toEntity(CharacterDTO entity) {
        return new Character().setName(entity.getName())
                .setId(entity.getId())
                .setDescription(entity.getDescription())
                .setImagePath(entity.getThumbnail().path + File.separator + imageSize + "." + entity.getThumbnail().extension);
    }
}
