package com.dnod.marvelapp.service.impl.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dnod.marvelapp.data.Character;
import com.dnod.marvelapp.service.IDBProvider;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class SQLProviderImpl extends SQLiteOpenHelper implements IDBProvider {

    private static final int VERSION = 1;

    enum CharacterTable {
        id, name, description, imagePath, ts
    }

    public static final String DATABASE_NAME = "marvelapp.db";
    public static final String CHARACTER_TABLE_NAME = "character";

    @Inject
    public SQLProviderImpl(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table " + CHARACTER_TABLE_NAME +
                        " (" + CharacterTable.id.name() + " text primary key, " +
                        CharacterTable.name.name() + " text, " +
                        CharacterTable.description.name() + " text, " +
                        CharacterTable.ts.name() + " integer, " +
                        CharacterTable.imagePath.name() + " text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CHARACTER_TABLE_NAME);
        onCreate(db);
    }

    @Override
    public List<Character> getLatestCharacters(int limit) {

        List<Character> result = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + CHARACTER_TABLE_NAME + " ORDER BY " + CharacterTable.ts.name() + " ASC LIMIT " + limit, null);
        res.moveToFirst();

        while (!res.isAfterLast()) {
            result.add(new Character()
                    .setName(res.getString(res.getColumnIndex(CharacterTable.name.name())))
                    .setDescription(res.getString(res.getColumnIndex(CharacterTable.description.name())))
                    .setImagePath(res.getString(res.getColumnIndex(CharacterTable.imagePath.name())))
                    .setId(res.getString(res.getColumnIndex(CharacterTable.id.name()))));
            res.moveToNext();
        }
        res.close();
        return result;
    }

    @Override
    public boolean saveCharacter(Character character) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CharacterTable.name.name(), character.getName());
        contentValues.put(CharacterTable.id.name(), character.getId());
        contentValues.put(CharacterTable.imagePath.name(), character.getImagePath());
        contentValues.put(CharacterTable.description.name(), character.getDescription());
        return db.insert(CHARACTER_TABLE_NAME, null, contentValues) >= 0;
    }
}
