package com.dnod.marvelapp.service;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public abstract class Marshaller<E, D> {

    public List<D> fromEntities(@NonNull List<? extends E> entities){
        List<D> res = new ArrayList<D>();
        for(E entity : entities)
            res.add(fromEntity(entity));
        return res;
    }

    public abstract D fromEntity(E entity);

    public List<E> toEntities(@NonNull List<? extends D> entities){
        List<E> res = new ArrayList<E>();
        for(D entity : entities)
            res.add(toEntity(entity));
        return res;
    }

    public abstract E toEntity(D entity);
}
