package com.dnod.marvelapp.service.impl.api;

import com.dnod.marvelapp.AppController;
import com.dnod.marvelapp.Endpoint;
import com.dnod.marvelapp.PrivateApiKey;
import com.dnod.marvelapp.PublickApiKey;
import com.dnod.marvelapp.R;
import com.dnod.marvelapp.data.Character;
import com.dnod.marvelapp.event.CharacterLoadedEvent;
import com.dnod.marvelapp.service.IBroadcast;
import com.dnod.marvelapp.service.IClientApi;
import com.dnod.marvelapp.service.IDBProvider;
import com.google.gson.Gson;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class OkHTTPApiImpl implements IClientApi {

    private final OkHttpClient client;
    private final CharacterDTOMarshaller dataMarshaller;
    private final Gson gson;
    private final String endpointUrl;
    private final String publicKey;
    private final String privateKey;
    private final IBroadcast broadcast;
    private final IDBProvider dbProvider;

    @Inject
    public OkHTTPApiImpl(@Endpoint String endpointUrl, @PublickApiKey String publicKey,
                         @PrivateApiKey String privateKey, CharacterDTOMarshaller dataMarshaller,
                         IBroadcast broadcast, IDBProvider dbProvider,
                         Gson gson) {
        this.dbProvider = dbProvider;
        this.client = new OkHttpClient();
        this.broadcast = broadcast;
        this.dataMarshaller = dataMarshaller;
        this.gson = gson;
        this.endpointUrl = endpointUrl;
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    @Override
    public void searchCharacter(String query) {
        long ts = System.currentTimeMillis();
        StringBuilder requestBuilder = new StringBuilder(endpointUrl).append("?");
        requestBuilder.append("apikey=").append(publicKey);
        requestBuilder.append("&hash=").append(MD5(ts + privateKey + publicKey));
        requestBuilder.append("&ts=").append(ts);
        requestBuilder.append("&name=").append(query);
        Observable.just(requestBuilder.toString())
                .subscribeOn(Schedulers.newThread())
                .map(new Func1<String, CharacterLoadedEvent>() {
                    @Override
                    public CharacterLoadedEvent call(String url) {
                        Character character = null;
                        String error = null;
                        Request request = new Request.Builder()
                                .url(url)
                                .build();
                        try {
                            okhttp3.Response response = client.newCall(request).execute();
                            if (response.isSuccessful()) {
                                Result result = gson.fromJson(response.body().charStream(), Result.class);
                                if (result.message != null) {
                                    error = AppController.getInstance().getString(R.string.api_error);
                                } else {
                                    if (result.data.results.size() == 0) {
                                        error = AppController.getInstance().getString(R.string.wrong_name_error);
                                    } else {
                                        //Show first item if more then one
                                        character = dataMarshaller.toEntity(result.data.results.get(0));
                                        dbProvider.saveCharacter(character);
                                    }
                                }
                            } else {
                                error = AppController.getInstance().getString(R.string.api_error);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            error = AppController.getInstance().getString(R.string.api_error);
                        }
                        if (error != null) {
                            return new CharacterLoadedEvent(error);
                        }
                        return new CharacterLoadedEvent(character);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<CharacterLoadedEvent>() {
                    @Override
                    public void call(CharacterLoadedEvent event) {
                        broadcast.postEvent(event);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        broadcast.postEvent(new CharacterLoadedEvent(throwable.getLocalizedMessage()));
                    }
                });
    }

    private String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (byte anArray : array) {
                sb.append(Integer.toHexString((anArray & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
