package com.dnod.marvelapp.service.impl.api;

public class Thumbnail {
    protected String path;
    protected String extension;

    public Thumbnail setPath(String path) {
        this.path = path;
        return this;
    }

    public Thumbnail setExtension(String extension) {
        this.extension = extension;
        return this;
    }
}
