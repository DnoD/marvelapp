package com.dnod.marvelapp;

import android.content.Context;

import com.dnod.marvelapp.service.IClientApi;
import com.dnod.marvelapp.service.impl.api.OkHTTPApiImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class NetworkModule {

    @Provides
    @Singleton
    public IClientApi provideClientApi(OkHTTPApiImpl api) {
        return api;
    }

    @Provides
    @Singleton
    public Gson provideGson() {
        return new GsonBuilder().create();
    }

    @Provides
    @Endpoint
    @Singleton
    public String provideEndpoint() {
        return "http://gateway.marvel.com:80/v1/public/characters";
    }

    @Provides
    @ImageRepresentationSize
    @Singleton
    public String provideImageRepresentationSize(Context context) {
        return context.getString(R.string.image_size);
    }

    @Provides
    @PublickApiKey
    @Singleton
    public String providePublicApiKey(Context context) {
        return context.getString(R.string.public_api_key);
    }

    @Provides
    @PrivateApiKey
    @Singleton
    public String providePrivateApiKey(Context context) {
        return context.getString(R.string.private_api_key);
    }
}
