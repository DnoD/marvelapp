package com.dnod.marvelapp;

import com.dnod.marvelapp.service.IDBProvider;
import com.dnod.marvelapp.service.impl.database.SQLProviderImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataStorageModule {

    @Provides
    @Singleton
    public IDBProvider provideDBProvider(SQLProviderImpl provider) {
        return provider;
    }
}
