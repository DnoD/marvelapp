package com.dnod.marvelapp.event;

import com.dnod.marvelapp.data.Character;

public class CharacterLoadedEvent {
    private final String error;
    private final Character character;

    public CharacterLoadedEvent(String error) {
        this.error = error;
        this.character = null;
    }

    public CharacterLoadedEvent(Character character) {
        this.error = null;
        this.character = character;
    }

    public String getError() {
        return error;
    }

    public Character getCharacter() {
        return character;
    }
}
