package com.dnod.marvelapp.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Character implements Parcelable {
    private String id;
    private String description;
    private String name;
    private String imagePath;

    public Character() {
    }

    public String getId() {
        return id;
    }

    public Character setId(String id) {
        this.id = id;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Character setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getName() {
        return name;
    }

    public Character setName(String name) {
        this.name = name;
        return this;
    }

    public String getImagePath() {
        return imagePath;
    }

    public Character setImagePath(String imagePath) {
        this.imagePath = imagePath;
        return this;
    }

    protected Character(Parcel in) {
        id = in.readString();
        description = in.readString();
        name = in.readString();
        imagePath = in.readString();
    }

    public static final Creator<Character> CREATOR = new Creator<Character>() {
        @Override
        public Character createFromParcel(Parcel in) {
            return new Character(in);
        }

        @Override
        public Character[] newArray(int size) {
            return new Character[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(description);
        parcel.writeString(name);
        parcel.writeString(imagePath);
    }
}
