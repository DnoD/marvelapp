package com.dnod.marvelapp;

import com.dnod.marvelapp.service.impl.api.CharacterDTOMarshaller;

import dagger.Module;
import dagger.Provides;

@Module
public class UtilsModule {

    @Provides
    public CharacterDTOMarshaller provideCharacterDTOMarshaller(@ImageRepresentationSize String imageSize) {
        return new CharacterDTOMarshaller(imageSize);
    }
}
