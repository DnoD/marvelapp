package com.dnod.marvelapp;

import com.dnod.marvelapp.ui.activity.MainActivity;
import com.dnod.marvelapp.ui.fragment.MainFragment;
import com.dnod.marvelapp.ui.loader.CachedCharactersLoader;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {
        ApplicationModule.class,
        NetworkModule.class,
        UtilsModule.class,
        DataStorageModule.class})
@Singleton
public interface ApplicationComponent {

    void inject(MainActivity activity);

    void inject(CachedCharactersLoader loader);

    void inject(MainFragment fragment);
}
