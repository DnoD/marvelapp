package com.dnod.marvelapp;

import android.content.Context;

import com.dnod.marvelapp.service.IBroadcast;
import com.dnod.marvelapp.service.impl.EventBusBroadcastImpl;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private Context context;

    public ApplicationModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    public Context provideAppContext() {
        return context;
    }

    @Provides
    @Singleton
    public IBroadcast provideBroadcast() {
        return new EventBusBroadcastImpl(EventBus.getDefault());
    }
}