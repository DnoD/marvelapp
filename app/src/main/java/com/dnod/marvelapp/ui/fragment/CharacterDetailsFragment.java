package com.dnod.marvelapp.ui.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.dnod.marvelapp.R;
import com.dnod.marvelapp.data.Character;
import com.dnod.marvelapp.databinding.FragmentCharacterDetailsBinding;

public class CharacterDetailsFragment extends BaseFragment {
    public static final String TAG = "CharacterDetailsFragment";

    private static final String PROVIDED_CHARACTER = "provided_character";

    public static CharacterDetailsFragment newInstance(Character character) {
        CharacterDetailsFragment fragment = new CharacterDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(PROVIDED_CHARACTER, character);
        fragment.setArguments(bundle);
        return fragment;
    }

    private Character character;
    private FragmentCharacterDetailsBinding bindingObject;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        bindingObject = DataBindingUtil.inflate(inflater, R.layout.fragment_character_details, container, false);
        character = getArguments().getParcelable(PROVIDED_CHARACTER);
        bindingObject.setModel(character);
        ((AppCompatActivity) getActivity()).setSupportActionBar(bindingObject.toolbar);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        Glide.with(getContext())
                .load(character.getImagePath())
                .fitCenter()
                .into(bindingObject.image);
        return bindingObject.getRoot();
    }
}
