package com.dnod.marvelapp.ui.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.dnod.marvelapp.AppController;
import com.dnod.marvelapp.R;
import com.dnod.marvelapp.data.Character;
import com.dnod.marvelapp.service.IDBProvider;

import java.util.List;

import javax.inject.Inject;

public class CachedCharactersLoader extends AsyncTaskLoader<CachedCharactersLoader.Result> {

    @Inject
    IDBProvider dbProvider;

    public CachedCharactersLoader(Context context) {
        super(context);
        AppController.getComponent().inject(this);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public Result loadInBackground() {
        int cacheSize = getContext().getResources().getInteger(R.integer.cache_size);
        Result result = new Result();
        result.characters = dbProvider.getLatestCharacters(cacheSize);
        return result;
    }

    public class Result {
        List<Character> characters;

        public List<Character> getCharacters() {
            return characters;
        }
    }
}
