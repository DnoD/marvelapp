package com.dnod.marvelapp.ui.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.dnod.marvelapp.AppController;
import com.dnod.marvelapp.R;
import com.dnod.marvelapp.data.Character;
import com.dnod.marvelapp.databinding.FragmentMainBinding;
import com.dnod.marvelapp.event.CharacterLoadedEvent;
import com.dnod.marvelapp.service.IBroadcast;
import com.dnod.marvelapp.service.IClientApi;
import com.dnod.marvelapp.ui.adapter.CharactersAdapter;
import com.dnod.marvelapp.ui.decor.DividerItemDecoration;
import com.dnod.marvelapp.ui.loader.CachedCharactersLoader;
import com.dnod.marvelapp.utils.Constants;
import com.dnod.marvelapp.utils.UIUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

public class MainFragment extends BaseFragment implements LoaderManager.LoaderCallbacks {
    private static final String FRAGMENT_STATE = "fragment_state";

    public static final String TAG = "MainFragment";

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    private CharactersAdapter adapter;
    private FragmentMainBinding bindingObject;
    @Inject
    IBroadcast broadcast;
    @Inject
    IClientApi clientApi;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        bindingObject = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);
        AppController.getComponent().inject(this);
        ((AppCompatActivity) getActivity()).setSupportActionBar(bindingObject.toolbar);
        bindingObject.btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchCharacter();
            }
        });
        bindingObject.characterName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchCharacter();
                    return true;
                }
                return false;
            }
        });
        bindingObject.characterName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (bindingObject.characterNameContainer.isErrorEnabled()) {
                    bindingObject.characterNameContainer.setErrorEnabled(false);
                }
            }
        });
        bindingObject.dataList.setLayoutManager(new LinearLayoutManager(getActivity()));
        bindingObject.dataList.addItemDecoration(new DividerItemDecoration(getActivity(),
                R.drawable.simple_divider));
        if (getArguments().containsKey(FRAGMENT_STATE)) {
            FragmentState fragmentState = getArguments().getParcelable(FRAGMENT_STATE);
            bindingObject.dataList.setAdapter(adapter = new CharactersAdapter(getContext(),
                    (CharactersAdapter.Listener) getActivity(),
                    fragmentState.dataList));
        } else {
            bindingObject.dataList.setAdapter(adapter = new CharactersAdapter(getContext(),
                    (CharactersAdapter.Listener) getActivity()));
            getLoaderManager().restartLoader(Constants.CACHED_DATA_LOADER_ID, null, this);
        }
        return bindingObject.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        broadcast.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        broadcast.unregister(this);
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        return new CachedCharactersLoader(getContext());
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        if (loader.getId() == Constants.CACHED_DATA_LOADER_ID) {
            CachedCharactersLoader.Result result = (CachedCharactersLoader.Result) data;
            if (result.getCharacters() != null) {
                adapter.replaceAll(result.getCharacters());
                getArguments().putParcelable(FRAGMENT_STATE, new FragmentState(result.getCharacters()));
            }
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleDataEvent(CharacterLoadedEvent event) {
        hideProgressDialog();
        if (event.getError() != null) {
            Snackbar.make(bindingObject.getRoot(), event.getError(), Snackbar.LENGTH_LONG)
                    .setAction(R.string.btn_retry, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            searchCharacter();
                        }
                    }).show();
        }
    }

    public boolean validateFields() {
        int errorsCount = 0;
        String characterName = bindingObject.characterName.getText().toString();
        if (TextUtils.isEmpty(characterName)) {
            bindingObject.characterNameContainer.setErrorEnabled(true);
            bindingObject.characterNameContainer.setError(getString(R.string.error_empty_field));
            errorsCount++;
        }

        return errorsCount == 0;
    }

    private void searchCharacter() {
        if (!validateFields()) {
            return;
        }
        UIUtil.hideKeyboard(getActivity());
        showProgressDialog("", getString(R.string.search_progress));
        clientApi.searchCharacter(bindingObject.characterName.getText().toString());
    }

    static final class FragmentState implements Parcelable {
        List<Character> dataList;

        public FragmentState(List<Character> characters) {
            this.dataList = characters;
        }

        protected FragmentState(Parcel in) {
            dataList = in.createTypedArrayList(Character.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(dataList);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<FragmentState> CREATOR = new Creator<FragmentState>() {
            @Override
            public FragmentState createFromParcel(Parcel in) {
                return new FragmentState(in);
            }

            @Override
            public FragmentState[] newArray(int size) {
                return new FragmentState[size];
            }
        };
    }
}
