package com.dnod.marvelapp.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.dnod.marvelapp.AppController;
import com.dnod.marvelapp.R;
import com.dnod.marvelapp.data.Character;
import com.dnod.marvelapp.databinding.ActivityMainBinding;
import com.dnod.marvelapp.event.CharacterLoadedEvent;
import com.dnod.marvelapp.service.IBroadcast;
import com.dnod.marvelapp.ui.adapter.CharactersAdapter;
import com.dnod.marvelapp.ui.fragment.CharacterDetailsFragment;
import com.dnod.marvelapp.ui.fragment.MainFragment;
import com.dnod.marvelapp.utils.UIUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements CharactersAdapter.Listener {

    @Inject
    IBroadcast broadcast;

    private ActivityMainBinding bindingObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppController.getComponent().inject(this);
        bindingObject = DataBindingUtil.setContentView(this, R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(bindingObject.fragmentContainer.getId(), MainFragment.newInstance(),
                            MainFragment.TAG).commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        broadcast.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        broadcast.unregister(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack();
                    return true;
                }
                break;
        }
        return false;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleDataEvent(CharacterLoadedEvent event) {
        if (event.getError() == null) {
            showCharacterScreen(event.getCharacter());
        }
    }

    @Override
    public void onItemClick(Character item) {
        showCharacterScreen(item);
    }

    private void showCharacterScreen(Character character) {
        UIUtil.hideKeyboard(this);
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(bindingObject.fragmentContainer.getId(), CharacterDetailsFragment.newInstance(character)).addToBackStack(null).commit();
    }
}
