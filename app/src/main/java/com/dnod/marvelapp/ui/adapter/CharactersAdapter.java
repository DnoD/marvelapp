package com.dnod.marvelapp.ui.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.dnod.marvelapp.R;
import com.dnod.marvelapp.data.Character;
import com.dnod.marvelapp.databinding.ItemCharacterBinding;

import java.util.ArrayList;
import java.util.List;

public class CharactersAdapter extends RecyclerView.Adapter<CharactersAdapter.Holder> {

    public interface Listener {
        void onItemClick(Character item);
    }

    protected final List<Character> items = new ArrayList<>();
    protected final Listener listener;
    protected final LayoutInflater layoutInflater;

    public CharactersAdapter(Context context, Listener listener, List<Character> items) {
        this(context, listener);
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public CharactersAdapter(Context context, Listener listener) {
        this.listener = listener;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void replaceAll(List<Character> data) {
        this.items.clear();
        this.items.addAll(data);
        notifyDataSetChanged();
    }

    public List<Character> getItems() {
        return items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder((ItemCharacterBinding) DataBindingUtil.inflate(layoutInflater,
                R.layout.item_character, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Character character = items.get(position);
        holder.bindingObject.setModel(character);
        Glide.with(layoutInflater.getContext())
                .load(character.getImagePath())
                .centerCrop().crossFade().into(holder.bindingObject.icon);
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ItemCharacterBinding bindingObject;

        public Holder(ItemCharacterBinding binding) {
            super(binding.getRoot());
            this.bindingObject = binding;
            this.bindingObject.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(items.get(getAdapterPosition()));
        }
    }
}
