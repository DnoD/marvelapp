package com.dnod.marvelapp.ui.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.dnod.marvelapp.ui.dialog.ProgressDialogFragment;

public abstract class BaseFragment extends Fragment {

    public void showProgressDialog(String title, String message){
        showProgressDialog(title, message, true);
    }

    public void showProgressDialog(String title, String message, boolean modal){
        ProgressDialogFragment.newInstance(title, message)
                .setModal(modal).show(getFragmentManager(), ProgressDialogFragment.TAG);
    }

    public void hideProgressDialog() {
        FragmentManager fragmentManager = getFragmentManager();
        if(fragmentManager != null) {
            Fragment prev = fragmentManager.findFragmentByTag(ProgressDialogFragment.TAG);
            if (prev != null) {
                ProgressDialogFragment df = (ProgressDialogFragment) prev;
                df.dismiss();
            }
        }
    }
}
