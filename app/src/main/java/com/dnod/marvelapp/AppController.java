package com.dnod.marvelapp;

import android.app.Application;
import android.content.Context;

public class AppController extends Application {

    private static AppController instance;

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(instance))
                .build();
    }

    public static ApplicationComponent getComponent() {
        return instance.component;
    }

    public static Context getInstance() {
        return instance;
    }
}
